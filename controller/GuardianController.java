package controller;

import java.util.List;
import java.util.ArrayList;
import model.entity.Guardian;
import model.GuardianDao;
import model.Database;

/**
 * Escreva a descrição da classe GuardianController aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class GuardianController
{
    private GuardianDao dao;

    public GuardianController(Database database)
    {
        this.dao = new GuardianDao(database);
    }

    public void create(Guardian guardian) 
    {
        dao.create(guardian);
    }    

    public List<Guardian> readAll() 
    {
        return dao.loadAll();
    }

    public void update(Guardian guardian) 
    {
        dao.update(guardian);
    }

    public void delete(Guardian guardian) 
    {
        dao.delete(guardian);
    }  
    
}
