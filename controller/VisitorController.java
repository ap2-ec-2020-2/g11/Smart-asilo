package controller;

import java.util.List;
import java.util.ArrayList;

import model.entity.Visitor;
import model.VisitorDao;
import model.Database;

public class VisitorController 
{
    private VisitorDao dao;

    public VisitorController(Database database)
    {
        this.dao = new VisitorDao(database);
    }

    public Visitor create(Visitor visitor) 
    {
        return dao.create(visitor);
    }    

    public List<Visitor> readAll() 
    {
        return dao.readAll();
    }

    public void update(Visitor visitor) 
    {
        dao.update(visitor);
    }
}