package controller;

import java.util.List;
import java.util.ArrayList;
import model.entity.Resident;
import model.ResidentDao;
import model.Database;

/**
 * Escreva a descrição da classe ResidentController aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class ResidentController
{
    private ResidentDao dao;

    public ResidentController(Database database)
    {
        this.dao = new ResidentDao(database);
    }

    public void create(Resident resident) 
    {
        dao.create(resident);
    }    

    public List<Resident> readAll() 
    {
        return dao.loadAll();
    }

    public void update(Resident resident) 
    {
        dao.update(resident);
    }

    public void delete(Resident resident) 
    {
        dao.delete(resident);
    }  
    
}
