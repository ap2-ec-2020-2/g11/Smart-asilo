package controller;

import java.util.List;
import java.util.ArrayList;
import model.entity.Maintenance;
import model.MaintenanceDao;
import model.Database;

public class MaintenanceController 
{
    private MaintenanceDao dao;

    public MaintenanceController(Database database)
    {
        this.dao = new MaintenanceDao(database);
    }

    public Maintenance create(Maintenance maintenance) 
    {
        return dao.create(maintenance);
    }    

    public List<Maintenance> readAll() 
    {
        return dao.readAll();
    }

    public void update(Maintenance maintenance) 
    {
        dao.update(maintenance);
    }
}