package controller;

import java.util.List;
import java.util.ArrayList;
import model.entity.Pharmacy;
import model.PharmacyDao;
import model.Database;

public class PharmacyController 
{
    private PharmacyDao dao;

    public PharmacyController(Database database)
    {
        this.dao = new PharmacyDao(database);
    }

    public Pharmacy create(Pharmacy pharmacy) 
    {
        return dao.create(pharmacy);
    }    

    public List<Pharmacy> readAll() 
    {
        return dao.readAll();
    }

    public void update(Pharmacy pharmacy) 
    {
        dao.update(pharmacy);
    }
}