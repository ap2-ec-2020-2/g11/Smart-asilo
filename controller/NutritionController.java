package controller;

import java.util.List;
import java.util.ArrayList;
import model.entity.Nutrition;
import model.NutritionDao;
import model.Database;

public class NutritionController 
{
    private NutritionDao dao;

    public NutritionController(Database database)
    {
        this.dao = new NutritionDao(database);
    }

    public Nutrition create(Nutrition nutrition) 
    {
        return dao.create(nutrition);
    }    

    public List<Nutrition> readAll() 
    {
        return dao.readAll();
    }

    public void update(Nutrition nutrition) 
    {
        dao.update(nutrition);
    }
}