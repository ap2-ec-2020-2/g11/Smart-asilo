package controller;

import java.util.List;
import java.util.ArrayList;
import model.entity.Professional;
import model.ProfessionalDao;
import model.Database;

/**
 * Escreva a descrição da classe ProfessionalController aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class ProfessionalController
{
    private ProfessionalDao dao;

    public ProfessionalController(Database database)
    {
        this.dao = new ProfessionalDao(database);
    }

    public void create(Professional professional) 
    {
        dao.create(professional);
    }    

    public List<Professional> readAll() 
    {
        return dao.loadAll();
    }

    public void update(Professional professional) 
    {
        dao.update(professional);
    }

    public void delete(Professional professional) 
    {
        dao.delete(professional);
    }  
    
}
