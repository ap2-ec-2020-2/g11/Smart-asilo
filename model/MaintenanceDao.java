package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.Maintenance;

public class MaintenanceDao
{  private static Database database;
    private static Dao<Maintenance, Integer> dao;
    private List<Maintenance> LoadedMaintenances;
    private Maintenance LoadedMaintenance; 

    public MaintenanceDao(Database database) 
    {
        this.setDatabase(database);
        LoadedMaintenances = new ArrayList<Maintenance>();
    }

    public static void setDatabase(Database database) 
    {
        MaintenanceDao.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Maintenance.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Maintenance.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    } 

    public Maintenance create(Maintenance maintenance) 
    {
        int nrows = 0;
        try {
            nrows = dao.create(maintenance);
            if ( nrows == 0 )
                throw new SQLException("Error: object not saved");
            this.LoadedMaintenance = maintenance;
            LoadedMaintenances.add(maintenance);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return maintenance;
    } 

    public Maintenance readFromNidc(int nidc) {
        try {
            this.LoadedMaintenance = dao.queryForId(nidc);
            if (this.LoadedMaintenance != null)
                this.LoadedMaintenances.add(this.LoadedMaintenance);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.LoadedMaintenance;
    }    

    public List<Maintenance> readAll() {
        try {
            this.LoadedMaintenances = dao.queryForAll();
            if (this.LoadedMaintenances.size() != 0)
                this.LoadedMaintenance = this.LoadedMaintenances.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.LoadedMaintenances;
    }  

    public void update(Maintenance maintenance) 
    {
        try
        {
            dao.update(maintenance);
        }
        catch (java.sql.SQLException e)
        {
            System.out.println(e);
        }
    }

    public void delete(Maintenance maintenance) 
    {
        try
        {
            dao.deleteById(maintenance.getNidc());
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }  

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie database*/
    public Database getDatabase(){
        return this.database;
    }//end method getDatabase

    /**GET Method Propertie dao*/
    public com.j256.ormlite.dao.Dao<model.entity.Maintenance, java.lang.Integer> getDao(){
        return this.dao;
    }//end method getDao

    /**SET Method Propertie dao*/
    public void setDao(com.j256.ormlite.dao.Dao<model.entity.Maintenance, java.lang.Integer> dao){
        this.dao = dao;
    }//end method setDao

    /**GET Method Propertie LoadedMaintenances*/
    public java.util.List<model.entity.Maintenance> getLoadedMaintenances(){
        return this.LoadedMaintenances;
    }//end method getLoadedMaintenances

    /**SET Method Propertie LoadedMaintenances*/
    public void setLoadedMaintenances(java.util.List<model.entity.Maintenance> LoadedMaintenances){
        this.LoadedMaintenances = LoadedMaintenances;
    }//end method setLoadedMaintenances

    /**GET Method Propertie LoadedMaintenance*/
    public Maintenance getLoadedMaintenance(){
        return this.LoadedMaintenance;
    }//end method getLoadedMaintenance

    /**SET Method Propertie LoadedMaintenance*/
    public void setLoadedMaintenance(Maintenance LoadedMaintenance){
        this.LoadedMaintenance = LoadedMaintenance;
    }//end method setLoadedMaintenance

    //End GetterSetterExtension Source Code
//!
}
