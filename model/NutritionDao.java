package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.Nutrition;

public class NutritionDao
{
    private static Database database;
    private static Dao<Nutrition, Integer> dao;
    private List<Nutrition> LoadedNutritions;
    private Nutrition LoadedNutrition; 

    public NutritionDao(Database database) 
    {
        this.setDatabase(database);
        LoadedNutritions = new ArrayList<Nutrition>();
    }

    public static void setDatabase(Database database) 
    {
        NutritionDao.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Nutrition.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Nutrition.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    } 

    public Nutrition create(Nutrition nutrition) 
    {
        int nrows = 0;
        try {
            nrows = dao.create(nutrition);
            if ( nrows == 0 )
                throw new SQLException("Error: object not saved");
            this.LoadedNutrition = nutrition;
            LoadedNutritions.add(nutrition);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return nutrition;
    } 

    public Nutrition readFromNidc(int nidc) {
        try {
            this.LoadedNutrition = dao.queryForId(nidc);
            if (this.LoadedNutrition != null)
                this.LoadedNutritions.add(this.LoadedNutrition);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.LoadedNutrition;
    }    

    public List<Nutrition> readAll() {
        try {
            this.LoadedNutritions = dao.queryForAll();
            if (this.LoadedNutritions.size() != 0)
                this.LoadedNutrition = this.LoadedNutritions.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.LoadedNutritions;
    }  

    public void update(Nutrition nutrition) 
    {
        try
        {
            dao.update(nutrition);
        }
        catch (java.sql.SQLException e)
        {
            System.out.println(e);
        }
    }

    public void delete(Nutrition nutrition) 
    {
        try
        {
            dao.deleteById(nutrition.getNidc());
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }  

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie database*/
    public Database getDatabase(){
        return this.database;
    }//end method getDatabase

    /**GET Method Propertie dao*/
    public com.j256.ormlite.dao.Dao<model.entity.Nutrition, java.lang.Integer> getDao(){
        return this.dao;
    }//end method getDao

    /**SET Method Propertie dao*/
    public void setDao(com.j256.ormlite.dao.Dao<model.entity.Nutrition, java.lang.Integer> dao){
        this.dao = dao;
    }//end method setDao

    /**GET Method Propertie LoadedNutritions*/
    public java.util.List<model.entity.Nutrition> getLoadedNutritions(){
        return this.LoadedNutritions;
    }//end method getLoadedNutritions

    /**SET Method Propertie LoadedNutritions*/
    public void setLoadedNutritions(java.util.List<model.entity.Nutrition> LoadedNutritions){
        this.LoadedNutritions = LoadedNutritions;
    }//end method setLoadedNutritions

    /**GET Method Propertie LoadedNutrition*/
    public Nutrition getLoadedNutrition(){
        return this.LoadedNutrition;
    }//end method getLoadedNutrition

    /**SET Method Propertie LoadedNutrition*/
    public void setLoadedNutrition(Nutrition LoadedNutrition){
        this.LoadedNutrition = LoadedNutrition;
    }//end method setLoadedNutrition

    //End GetterSetterExtension Source Code
//!
}
