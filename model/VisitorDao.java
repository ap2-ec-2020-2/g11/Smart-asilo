/*não é necessário um delete, pois não se pode deletar quem visitou o asilo (por questões de segurança)*/

package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.Visitor;

public class VisitorDao
{
    private static Database database;
    private static Dao<Visitor, Integer> dao;
    private List<Visitor> loadedVisitors;
    private Visitor loadedVisitor; 

    public VisitorDao(Database database) 
    {
        this.setDatabase(database);
        loadedVisitors = new ArrayList<Visitor>();
    }

    public static void setDatabase(Database database) 
    {
        VisitorDao.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Visitor.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Visitor.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    } 

    public Visitor create(Visitor visitor) 
    {
        int nrows = 0;
        try {
            nrows = dao.create(visitor);
            if ( nrows == 0 )
                throw new SQLException("Error: object not saved");
            this.loadedVisitor = visitor;
            loadedVisitors.add(visitor);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return visitor;
    } 

    public Visitor readFromNidc(int nidc) 
    {
        try {
            this.loadedVisitor = dao.queryForId(nidc);
            if (this.loadedVisitor != null)
                this.loadedVisitors.add(this.loadedVisitor);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedVisitor;
    }    

    public List<Visitor> readAll() 
    {
        try {
            this.loadedVisitors = dao.queryForAll();
            if (this.loadedVisitors.size() != 0)
                this.loadedVisitor = this.loadedVisitors.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedVisitors;
    }  

    public void update(Visitor visitor) 
    {
        try
        {
            dao.update(visitor);
        }
        catch (java.sql.SQLException e)
        {
            System.out.println(e);
        }
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie database*/
    public Database getDatabase(){
        return this.database;
    }//end method getDatabase

    /**GET Method Propertie dao*/
    public com.j256.ormlite.dao.Dao<model.entity.Visitor, java.lang.Integer> getDao(){
        return this.dao;
    }//end method getDao

    /**SET Method Propertie dao*/
    public void setDao(com.j256.ormlite.dao.Dao<model.entity.Visitor, java.lang.Integer> dao){
        this.dao = dao;
    }//end method setDao

    /**GET Method Propertie loadedVisitors*/
    public java.util.List<model.entity.Visitor> getLoadedVisitors(){
        return this.loadedVisitors;
    }//end method getLoadedVisitors

    /**SET Method Propertie loadedVisitors*/
    public void setLoadedVisitors(java.util.List<model.entity.Visitor> loadedVisitors){
        this.loadedVisitors = loadedVisitors;
    }//end method setLoadedVisitors

    /**GET Method Propertie loadedVisitor*/
    public Visitor getLoadedVisitor(){
        return this.loadedVisitor;
    }//end method getLoadedVisitor

    /**SET Method Propertie loadedVisitor*/
    public void setLoadedVisitor(Visitor loadedVisitor){
        this.loadedVisitor = loadedVisitor;
    }//end method setLoadedVisitor

    //End GetterSetterExtension Source Code
//!
}