package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.Resident;
import model.entity.ResidentGuardian;

public class ResidentDao
{
    private static Database database;
    private static Dao<Resident, Integer> dao;
    private List<Resident> loadedResidents;
    private Resident loadedResident; 
    ArrayList<String> nidcs = new ArrayList<String>();

    public void loadFirst() throws Exception {
        if ( loadedResident == null || loadedResidents == null || loadedResidents.size() == 0)
            throw new Exception("Residents not loaded");
        setLoadedResident(loadedResidents.get(0));
    }    
    
    public void loadPrevious() throws Exception {
        if ( loadedResident == null || loadedResidents == null || loadedResidents.size() == 0)
            throw new Exception("Residents not loaded");
        int index = loadedResidents.indexOf(loadedResident);
        if ( index == 0 )
            throw new Exception("Could not loaded previous Resident, current index is 0");
        setLoadedResident(loadedResidents.get(index-1));
    }

    public void loadNext() throws Exception {
        if ( loadedResident == null || loadedResidents == null || loadedResidents.size() == 0)
            throw new Exception("Residents not loaded");
        int index = loadedResidents.indexOf(loadedResident);
        if ( index == loadedResidents.size()-1 )
            throw new Exception("Could not loaded next Resident, current index is the last");
        setLoadedResident(loadedResidents.get(index+1));
    }
    
    public void loadLast() throws Exception {
        if ( loadedResident == null || loadedResidents == null || loadedResidents.size() == 0)
            throw new Exception("Residents not loaded");
        int last = loadedResidents.size()-1;
        setLoadedResident(loadedResidents.get(last));
    }        
    
    public boolean loadedLast() throws Exception {
        if ( loadedResident == null || loadedResidents == null || loadedResidents.size() == 0)
            throw new Exception("Residents not loaded");
        if ( loadedResidents.indexOf(loadedResident)+1 == loadedResidents.size() )
            return true;
        else
            return false;
    }
    
    public boolean loadedFirst() throws Exception {
        if ( loadedResident == null || loadedResidents == null || loadedResidents.size() == 0)
            throw new Exception("Residents not loaded");
        if ( loadedResidents.indexOf(loadedResident) == 0 )
            return true;
        else
            return false;
    }
    
    public ResidentDao(Database database) 
    {
        this.setDatabase(database);
        loadedResidents = new ArrayList<Resident>();
    }

    public static void setDatabase(Database database) 
    {
        ResidentDao.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Resident.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Resident.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    } 

    public int create(Resident resident) 
    {
        int nrows = 0;
        try {
            if (dao.queryForId(resident.getNidc()) != null)
                throw new SQLException("Error: object already saved");
            else {
                nrows = dao.create(resident);
                if ( nrows == 0 )
                    throw new SQLException("Error: object not saved");
                this.loadedResident = resident;
                loadedResidents.add(resident);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return nrows;
    } 

    public int update(Resident resident) {
        int nrows = 0;
        try {
            if (dao.queryForId(resident.getNidc()) != null)
                throw new SQLException("Error: object already saved");
            else {
                nrows = dao.update(resident); 
                if ( nrows == 0 )
                    throw new SQLException("Error: object not updated");
                this.loadedResident.setNidc(resident.getNidc());
                this.loadedResident.setFullName(resident.getFullName());
                this.loadedResident.setBirthday(resident.getBirthday());
                this.loadedResident.setFingerprint(resident.getFingerprint());
                //this.loadedResident.setGuardian_nidc(resident.getGuardian_nidc());
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return nrows;
    }

    public int delete(Resident resident) 
    {
        int nrows = 0;
        try {
            if (dao.queryForId(resident.getNidc()) != null)
                throw new SQLException("Error: object already saved");
            else {
                nrows = dao.delete(resident);
                if ( nrows == 0 )
                    throw new SQLException("Error: object not deleted"); 
                int index = this.loadedResidents.indexOf(this.loadedResident);
                this.loadedResidents.remove(this.loadedResident);
            }
        } catch (SQLException e) {
             System.out.println(e);
        }
        return nrows;
    }   

    public Resident loadFromNidc(int nidc) {
        try {
            this.loadedResident = dao.queryForId(nidc);
            if (this.loadedResident != null)
                this.loadedResidents.add(this.loadedResident);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedResident;
    }    
    
    public List<Resident> loadAll() {
        try {
            this.loadedResidents =  dao.queryForAll();
            if (this.loadedResidents.size() != 0)
                this.loadedResident = this.loadedResidents.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedResidents;
    }
    

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie database*/
    public Database getDatabase(){
        return this.database;
    }//end method getDatabase

    /**GET Method Propertie dao*/
    public com.j256.ormlite.dao.Dao<model.entity.Resident, java.lang.Integer> getDao(){
        return this.dao;
    }//end method getDao

    /**SET Method Propertie dao*/
    public void setDao(com.j256.ormlite.dao.Dao<model.entity.Resident, java.lang.Integer> dao){
        this.dao = dao;
    }//end method setDao

    /**GET Method Propertie loadedResidents*/
    public java.util.List<model.entity.Resident> getLoadedResidents(){
        return this.loadedResidents;
    }//end method getLoadedResidents

    /**SET Method Propertie loadedResidents*/
    public void setLoadedResidents(java.util.List<model.entity.Resident> loadedResidents){
        this.loadedResidents = loadedResidents;
    }//end method setLoadedResidents

    /**GET Method Propertie loadedResident*/
    public Resident getLoadedResident(){
        return this.loadedResident;
    }//end method getLoadedResident

    /**SET Method Propertie loadedResident*/
    public void setLoadedResident(Resident loadedResident){
        this.loadedResident = loadedResident;
    }//end method setLoadedResident

    /**GET Method Propertie nidcs*/
    public java.util.ArrayList<java.lang.String> getNidcs(){
        return this.nidcs;
    }//end method getNidcs

    /**SET Method Propertie nidcs*/
    public void setNidcs(java.util.ArrayList<java.lang.String> nidcs){
        this.nidcs = nidcs;
    }//end method setNidcs

    //End GetterSetterExtension Source Code
//!
}