package model.entity;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;

public class ResidentGuardian
{
    @DatabaseField (id = true)
    public int guardian_nidc;
    //NICD - National ID card
    
    @DatabaseField(canBeNull = false, foreign = true)
    private Resident resident;
    
    @DatabaseField(canBeNull = false, foreign = true)
    private Guardian guardian;

    public void ResidentGuardian(Guardian g, Resident r)
    {
        guardian = g;        
        resident = r;
    }    

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie guardian_nidc*/
    public int getGuardian_nidc(){
        return this.guardian_nidc;
    }//end method getGuardian_nidc

    /**SET Method Propertie guardian_nidc*/
    public void setGuardian_nidc(int guardian_nidc){
        this.guardian_nidc = guardian_nidc;
    }//end method setGuardian_nidc

    /**GET Method Propertie resident*/
    public Resident getResident(){
        return this.resident;
    }//end method getResident

    /**SET Method Propertie resident*/
    public void setResident(Resident resident){
        this.resident = resident;
    }//end method setResident

    /**GET Method Propertie guardian*/
    public Guardian getGuardian(){
        return this.guardian;
    }//end method getGuardian

    /**SET Method Propertie guardian*/
    public void setGuardian(Guardian guardian){
        this.guardian = guardian;
    }//end method setGuardian

    //End GetterSetterExtension Source Code
//!
}