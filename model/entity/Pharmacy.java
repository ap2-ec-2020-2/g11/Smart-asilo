package model.entity;


import java.util.Date;
import java.text.SimpleDateFormat;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;

@DatabaseTable(tableName = "Pharmacy")
public class Pharmacy
{   
    @DatabaseField (id = true)
    private int nidc;
    //NICD - National ID card
    
    @DatabaseField
    private String materialName;
    
    @DatabaseField
    private String materialType;
    
    @DatabaseField
    private String details;
    
    @DatabaseField
    private String company;
    
    @DatabaseField
    private String materialGroup;
    
    @DatabaseField
    private int registration;
    
    @DatabaseField
    private int amount;
    
    @DatabaseField(dataType=DataType.DATE)
    private Date shelfLife;    
    


    //Start GetterSetterExtension Source Code
    /**GET Method Propertie nidc*/
    public int getNidc(){
        return this.nidc;
    }//end method getNidc

    /**SET Method Propertie nidc*/
    public void setNidc(int nidc){
        this.nidc = nidc;
    }//end method setNidc

    /**GET Method Propertie materialName*/
    public String getMaterialName(){
        return this.materialName;
    }//end method getMaterialName

    /**SET Method Propertie materialName*/
    public void setMaterialName(String materialName){
        this.materialName = materialName;
    }//end method setMaterialName

    /**GET Method Propertie materialType*/
    public String getMaterialType(){
        return this.materialType;
    }//end method getMaterialType

    /**SET Method Propertie materialType*/
    public void setMaterialType(String materialType){
        this.materialType = materialType;
    }//end method setMaterialType

    /**GET Method Propertie details*/
    public String getDetails(){
        return this.details;
    }//end method getDetails

    /**SET Method Propertie details*/
    public void setDetails(String details){
        this.details = details;
    }//end method setDetails

    /**GET Method Propertie company*/
    public String getCompany(){
        return this.company;
    }//end method getCompany

    /**SET Method Propertie company*/
    public void setCompany(String company){
        this.company = company;
    }//end method setCompany

    /**GET Method Propertie materialGroup*/
    public String getMaterialGroup(){
        return this.materialGroup;
    }//end method getMaterialGroup

    /**SET Method Propertie materialGroup*/
    public void setMaterialGroup(String materialGroup){
        this.materialGroup = materialGroup;
    }//end method setMaterialGroup

    /**GET Method Propertie registration*/
    public int getRegistration(){
        return this.registration;
    }//end method getRegistration

    /**SET Method Propertie registration*/
    public void setRegistration(int registration){
        this.registration = registration;
    }//end method setRegistration

    /**GET Method Propertie amount*/
    public int getAmount(){
        return this.amount;
    }//end method getAmount

    /**SET Method Propertie amount*/
    public void setAmount(int amount){
        this.amount = amount;
    }//end method setAmount

    /**GET Method Propertie shelfLife*/
    public Date getShelfLife(){
        return this.shelfLife;
    }//end method getShelfLife

    /**SET Method Propertie shelfLife*/
    public void setShelfLife(Date shelfLife){
        this.shelfLife = shelfLife;
    }//end method setShelfLife

    //End GetterSetterExtension Source Code
//!
}
