package model.entity;

import java.util.Date;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;

@DatabaseTable(tableName = "visitor")
public class Visitor
{
    @DatabaseField (id = true)
    private int nidc;
    //NICD - National ID card

    @DatabaseField
    private String fullName;

    @DatabaseField
    private String cpf;
    
    @DatabaseField
    private String rg;
    
    @DatabaseField
    private String phoneNumber;
    
    @DatabaseField
    private String residentfullName;
    
    @DatabaseField
    private String residentCpf;

    @DatabaseField
    private java.util.Date arrivalTime;
    
    @DatabaseField
    private java.util.Date departureTime;


    //Start GetterSetterExtension Source Code
    /**GET Method Propertie nidc*/
    public int getNidc(){
        return this.nidc;
    }//end method getNidc

    /**SET Method Propertie nidc*/
    public void setNidc(int nidc){
        this.nidc = nidc;
    }//end method setNidc

    /**GET Method Propertie fullName*/
    public String getFullName(){
        return this.fullName;
    }//end method getFullName

    /**SET Method Propertie fullName*/
    public void setFullName(String fullName){
        this.fullName = fullName;
    }//end method setFullName

    /**GET Method Propertie cpf*/
    public String getCpf(){
        return this.cpf;
    }//end method getCpf

    /**SET Method Propertie cpf*/
    public void setCpf(String cpf){
        this.cpf = cpf;
    }//end method setCpf

    /**GET Method Propertie rg*/
    public String getRg(){
        return this.rg;
    }//end method getRg

    /**SET Method Propertie rg*/
    public void setRg(String rg){
        this.rg = rg;
    }//end method setRg

    /**GET Method Propertie phoneNumber*/
    public String getPhoneNumber(){
        return this.phoneNumber;
    }//end method getPhoneNumber

    /**SET Method Propertie phoneNumber*/
    public void setPhoneNumber(String phoneNumber){
        this.phoneNumber = phoneNumber;
    }//end method setPhoneNumber

    /**GET Method Propertie residentfullName*/
    public String getResidentfullName(){
        return this.residentfullName;
    }//end method getResidentfullName

    /**SET Method Propertie residentfullName*/
    public void setResidentfullName(String residentfullName){
        this.residentfullName = residentfullName;
    }//end method setResidentfullName

    /**GET Method Propertie residentCpf*/
    public String getResidentCpf(){
        return this.residentCpf;
    }//end method getResidentCpf

    /**SET Method Propertie residentCpf*/
    public void setResidentCpf(String residentCpf){
        this.residentCpf = residentCpf;
    }//end method setResidentCpf

    /**GET Method Propertie arrivalTime*/
    public Date getArrivalTime(){
        return this.arrivalTime;
    }//end method getArrivalTime

    /**SET Method Propertie arrivalTime*/
    public void setArrivalTime(Date arrivalTime){
        this.arrivalTime = arrivalTime;
    }//end method setArrivalTime

    /**GET Method Propertie departureTime*/
    public Date getDepartureTime(){
        return this.departureTime;
    }//end method getDepartureTime

    /**SET Method Propertie departureTime*/
    public void setDepartureTime(Date departureTime){
        this.departureTime = departureTime;
    }//end method setDepartureTime

    //End GetterSetterExtension Source Code
    //!
}