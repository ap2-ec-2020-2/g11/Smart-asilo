package model.entity;

import java.util.Date;
import java.text.SimpleDateFormat;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;

@DatabaseTable(tableName = "resident")
public class Resident
{   
    @DatabaseField (id = true)
    private int nidc;
    //NICD - National ID card
    
    @DatabaseField
    private String fullName;
    
    @DatabaseField(dataType=DataType.DATE)
    private Date birthday;    
    
    private String printBirthday() {
        SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
        return dateFor.format(birthday);
    }
    
    @DatabaseField
    private String fingerprint;
    
    //Start GetterSetterExtension Source Code
    /**GET Method Propertie nidc*/
    public int getNidc(){
        return this.nidc;
    }//end method getNidc

    /**SET Method Propertie nidc*/
    public void setNidc(int nidc){
        this.nidc = nidc;
    }//end method setNidc

    /**GET Method Propertie fullName*/
    public String getFullName(){
        return this.fullName;
    }//end method getFullName

    /**SET Method Propertie fullName*/
    public void setFullName(String fullName){
        this.fullName = fullName;
    }//end method setFullName

    /**GET Method Propertie birthday*/
    public Date getBirthday(){
        return this.birthday;
    }//end method getBirthday

    /**SET Method Propertie birthday*/
    public void setBirthday(Date birthday){
        this.birthday = birthday;
    }//end method setBirthday

    /**GET Method Propertie fingerprint*/
    public String getFingerprint(){
        return this.fingerprint;
    }//end method getFingerprint

    /**SET Method Propertie fingerprint*/
    public void setFingerprint(String fingerprint){
        this.fingerprint = fingerprint;
    }//end method setFingerprint

    //End GetterSetterExtension Source Code
//!
}