package model.entity;

import java.util.Date;
import java.text.SimpleDateFormat;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;

@DatabaseTable(tableName = "guardian")
public class Guardian
{   
    @DatabaseField (id = true)
    public int guardian_nidc;
    //NICD - National ID card
    
    @DatabaseField
    private String fullName;
    
    @DatabaseField(dataType=DataType.DATE)
    private Date birthday;    
    
    private String printBirthday() {
        SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
        return dateFor.format(birthday);
    }
    
    @DatabaseField
    private int guardian_phone;
    
    @DatabaseField
    private String fingerprint;


    //Start GetterSetterExtension Source Code
    /**GET Method Propertie guardian_nidc*/
    public int getGuardian_nidc(){
        return this.guardian_nidc;
    }//end method getGuardian_nidc

    /**SET Method Propertie guardian_nidc*/
    public void setGuardian_nidc(int guardian_nidc){
        this.guardian_nidc = guardian_nidc;
    }//end method setGuardian_nidc

    /**GET Method Propertie fullName*/
    public String getFullName(){
        return this.fullName;
    }//end method getFullName

    /**SET Method Propertie fullName*/
    public void setFullName(String fullName){
        this.fullName = fullName;
    }//end method setFullName

    /**GET Method Propertie birthday*/
    public Date getBirthday(){
        return this.birthday;
    }//end method getBirthday

    /**SET Method Propertie birthday*/
    public void setBirthday(Date birthday){
        this.birthday = birthday;
    }//end method setBirthday

    /**GET Method Propertie guardian_phone*/
    public int getGuardian_phone(){
        return this.guardian_phone;
    }//end method getGuardian_phone

    /**SET Method Propertie guardian_phone*/
    public void setGuardian_phone(int guardian_phone){
        this.guardian_phone = guardian_phone;
    }//end method setGuardian_phone

    /**GET Method Propertie fingerprint*/
    public String getFingerprint(){
        return this.fingerprint;
    }//end method getFingerprint

    /**SET Method Propertie fingerprint*/
    public void setFingerprint(String fingerprint){
        this.fingerprint = fingerprint;
    }//end method setFingerprint

    //End GetterSetterExtension Source Code
//!
}