package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.Pharmacy;

public class PharmacyDao
{
    private static Database database;
    private static Dao<Pharmacy, Integer> dao;
    private List<Pharmacy> LoadedPharmacys;
    private Pharmacy LoadedPharmacy; 

    public PharmacyDao(Database database) 
    {
        this.setDatabase(database);
        LoadedPharmacys = new ArrayList<Pharmacy>();
    }

    public static void setDatabase(Database database) 
    {
        PharmacyDao.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Pharmacy.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Pharmacy.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    } 

    public Pharmacy create(Pharmacy pharmacy) 
    {
        int nrows = 0;
        try {
            nrows = dao.create(pharmacy);
            if ( nrows == 0 )
                throw new SQLException("Error: object not saved");
            this.LoadedPharmacy = pharmacy;
            LoadedPharmacys.add(pharmacy);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return pharmacy;
    } 

    public Pharmacy readFromNidc(int nidc) {
        try {
            this.LoadedPharmacy = dao.queryForId(nidc);
            if (this.LoadedPharmacy != null)
                this.LoadedPharmacys.add(this.LoadedPharmacy);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.LoadedPharmacy;
    }    

    public List<Pharmacy> readAll() {
        try {
            this.LoadedPharmacys = dao.queryForAll();
            if (this.LoadedPharmacys.size() != 0)
                this.LoadedPharmacy = this.LoadedPharmacys.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.LoadedPharmacys;
    }  

    public void update(Pharmacy pharmacy) 
    {
        try
        {
            dao.update(pharmacy);
        }
        catch (java.sql.SQLException e)
        {
            System.out.println(e);
        }
    }

    public void delete(Pharmacy pharmacy) 
    {
        try
        {
            dao.deleteById(pharmacy.getNidc());
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }  

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie database*/
    public Database getDatabase(){
        return this.database;
    }//end method getDatabase

    /**GET Method Propertie dao*/
    public com.j256.ormlite.dao.Dao<model.entity.Pharmacy, java.lang.Integer> getDao(){
        return this.dao;
    }//end method getDao

    /**SET Method Propertie dao*/
    public void setDao(com.j256.ormlite.dao.Dao<model.entity.Pharmacy, java.lang.Integer> dao){
        this.dao = dao;
    }//end method setDao

    /**GET Method Propertie LoadedPharmacys*/
    public java.util.List<model.entity.Pharmacy> getLoadedPharmacys(){
        return this.LoadedPharmacys;
    }//end method getLoadedPharmacys

    /**SET Method Propertie LoadedPharmacys*/
    public void setLoadedPharmacys(java.util.List<model.entity.Pharmacy> LoadedPharmacys){
        this.LoadedPharmacys = LoadedPharmacys;
    }//end method setLoadedPharmacys

    /**GET Method Propertie LoadedPharmacy*/
    public Pharmacy getLoadedPharmacy(){
        return this.LoadedPharmacy;
    }//end method getLoadedPharmacy

    /**SET Method Propertie LoadedPharmacy*/
    public void setLoadedPharmacy(Pharmacy LoadedPharmacy){
        this.LoadedPharmacy = LoadedPharmacy;
    }//end method setLoadedPharmacy

    //End GetterSetterExtension Source Code
//!
}
