package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.Professional;

public class ProfessionalDao
{
    private static Database database;
    private static Dao<Professional, Integer> dao;
    private List<Professional> loadedProfessionals;
    private Professional loadedProfessional; 
    ArrayList<String> nidcs = new ArrayList<String>();

    public void loadFirst() throws Exception {
        if ( loadedProfessional == null || loadedProfessionals == null || loadedProfessionals.size() == 0)
            throw new Exception("Professionals not loaded");
        setLoadedProfessional(loadedProfessionals.get(0));
    }    
    
    public void loadPrevious() throws Exception {
        if ( loadedProfessional == null || loadedProfessionals == null || loadedProfessionals.size() == 0)
            throw new Exception("Professionals not loaded");
        int index = loadedProfessionals.indexOf(loadedProfessional);
        if ( index == 0 )
            throw new Exception("Could not loaded previous professional, current index is 0");
        setLoadedProfessional(loadedProfessionals.get(index-1));
    }

    public void loadNext() throws Exception {
        if ( loadedProfessional == null || loadedProfessionals == null || loadedProfessionals.size() == 0)
            throw new Exception("Professionals not loaded");
        int index = loadedProfessionals.indexOf(loadedProfessional);
        if ( index == loadedProfessionals.size()-1 )
            throw new Exception("Could not loaded next professional, current index is the last");
        setLoadedProfessional(loadedProfessionals.get(index+1));
    }
    
    public void loadLast() throws Exception {
        if ( loadedProfessional == null || loadedProfessionals == null || loadedProfessionals.size() == 0)
            throw new Exception("Professionals not loaded");
        int last = loadedProfessionals.size()-1;
        setLoadedProfessional(loadedProfessionals.get(last));
    }        
    
    public boolean loadedLast() throws Exception {
        if ( loadedProfessional == null || loadedProfessionals == null || loadedProfessionals.size() == 0)
            throw new Exception("Professionals not loaded");
        if ( loadedProfessionals.indexOf(loadedProfessional)+1 == loadedProfessionals.size() )
            return true;
        else
            return false;
    }
    
    public boolean loadedFirst() throws Exception {
        if ( loadedProfessional == null || loadedProfessionals == null || loadedProfessionals.size() == 0)
            throw new Exception("Professionals not loaded");
        if ( loadedProfessionals.indexOf(loadedProfessional) == 0 )
            return true;
        else
            return false;
    }
    
    public ProfessionalDao(Database database) 
    {
        this.setDatabase(database);
        loadedProfessionals = new ArrayList<Professional>();
    }

    public static void setDatabase(Database database) 
    {
        ProfessionalDao.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Professional.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Professional.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    } 

    public int create(Professional professional) 
    {
        int nrows = 0;
        try {
            if (dao.queryForId(professional.getNidc()) != null)
                throw new SQLException("Error: object already saved");
            else {
                nrows = dao.create(professional);
                if ( nrows == 0 )
                    throw new SQLException("Error: object not saved");
                this.loadedProfessional = professional;
                loadedProfessionals.add(professional);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return nrows;
    } 

    public int update(Professional professional) {
        int nrows = 0;
        try {
            if (dao.queryForId(professional.getNidc()) != null)
                throw new SQLException("Error: object already saved");
            else {
                nrows = dao.update(professional); 
                if ( nrows == 0 )
                    throw new SQLException("Error: object not updated");
                this.loadedProfessional.setNidc(professional.getNidc());
                this.loadedProfessional.setFullName(professional.getFullName());
                this.loadedProfessional.setBirthday(professional.getBirthday());
                this.loadedProfessional.setPhone(professional.getPhone());
                this.loadedProfessional.setOccupational_area(professional.getOccupational_area());
                this.loadedProfessional.setFingerprint(professional.getFingerprint());
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return nrows;
    }

    public int delete(Professional professional) 
    {
        int nrows = 0;
        try {
            if (dao.queryForId(professional.getNidc()) != null)
                throw new SQLException("Error: object already saved");
            else {
                nrows = dao.delete(professional);
                if ( nrows == 0 )
                    throw new SQLException("Error: object not deleted"); 
                int index = this.loadedProfessionals.indexOf(this.loadedProfessional);
                this.loadedProfessionals.remove(this.loadedProfessional);
            }
        } catch (SQLException e) {
             System.out.println(e);
        }
        return nrows;
    }   

    public Professional loadFromNidc(int nidc) {
        try {
            this.loadedProfessional = dao.queryForId(nidc);
            if (this.loadedProfessional != null)
                this.loadedProfessionals.add(this.loadedProfessional);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedProfessional;
    }    
    
    public List<Professional> loadAll() {
        try {
            this.loadedProfessionals =  dao.queryForAll();
            if (this.loadedProfessionals.size() != 0)
                this.loadedProfessional = this.loadedProfessionals.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedProfessionals;
    }
    
    //Start GetterSetterExtension Source Code
    /**GET Method Propertie database*/
    public Database getDatabase(){
        return this.database;
    }//end method getDatabase

    /**SET Method Propertie database*/
    public void setDatabase(){
        this.database = database;
    }//end method setDatabase

    /**GET Method Propertie dao*/
    public com.j256.ormlite.dao.Dao<model.entity.Professional, java.lang.Integer> getDao(){
        return this.dao;
    }//end method getDao

    /**SET Method Propertie dao*/
    public void setDao(com.j256.ormlite.dao.Dao<model.entity.Professional, java.lang.Integer> dao){
        this.dao = dao;
    }//end method setDao

    /**GET Method Propertie loadedProfessionals*/
    public java.util.List<model.entity.Professional> getLoadedProfessionals(){
        return this.loadedProfessionals;
    }//end method getLoadedProfessionals

    /**SET Method Propertie loadedProfessionals*/
    public void setLoadedProfessionals(java.util.List<model.entity.Professional> loadedProfessionals){
        this.loadedProfessionals = loadedProfessionals;
    }//end method setLoadedProfessionals

    /**GET Method Propertie loadedProfessional*/
    public Professional getLoadedProfessional(){
        return this.loadedProfessional;
    }//end method getLoadedProfessional

    /**SET Method Propertie loadedProfessional*/
    public void setLoadedProfessional(Professional loadedProfessional){
        this.loadedProfessional = loadedProfessional;
    }//end method setLoadedProfessional

    /**GET Method Propertie nidcs*/
    public java.util.ArrayList<java.lang.String> getNidcs(){
        return this.nidcs;
    }//end method getNidcs

    /**SET Method Propertie nidcs*/
    public void setNidcs(java.util.ArrayList<java.lang.String> nidcs){
        this.nidcs = nidcs;
    }//end method setNidcs

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    //End GetterSetterExtension Source Code
//!
}