package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.Guardian;

public class GuardianDao
{
    private static Database database;
    private static Dao<Guardian, Integer> dao;
    private List<Guardian> loadedGuardians;
    private Guardian loadedGuardian; 
    ArrayList<String> nidcs = new ArrayList<String>();

    public void loadFirst() throws Exception {
        if ( loadedGuardian == null || loadedGuardians == null || loadedGuardians.size() == 0)
            throw new Exception("Guardians not loaded");
        setLoadedGuardian(loadedGuardians.get(0));
    }    
    
    public void loadPrevious() throws Exception {
        if ( loadedGuardian == null || loadedGuardians == null || loadedGuardians.size() == 0)
            throw new Exception("Guardians not loaded");
        int index = loadedGuardians.indexOf(loadedGuardian);
        if ( index == 0 )
            throw new Exception("Could not loaded previous Guardian, current index is 0");
        setLoadedGuardian(loadedGuardians.get(index-1));
    }

    public void loadNext() throws Exception {
        if ( loadedGuardian == null || loadedGuardians == null || loadedGuardians.size() == 0)
            throw new Exception("Guardians not loaded");
        int index = loadedGuardians.indexOf(loadedGuardian);
        if ( index == loadedGuardians.size()-1 )
            throw new Exception("Could not loaded next Guardian, current index is the last");
        setLoadedGuardian(loadedGuardians.get(index+1));
    }
    
    public void loadLast() throws Exception {
        if ( loadedGuardian == null || loadedGuardians == null || loadedGuardians.size() == 0)
            throw new Exception("Guardians not loaded");
        int last = loadedGuardians.size()-1;
        setLoadedGuardian(loadedGuardians.get(last));
    }        
    
    public boolean loadedLast() throws Exception {
        if ( loadedGuardian == null || loadedGuardians == null || loadedGuardians.size() == 0)
            throw new Exception("Guardians not loaded");
        if ( loadedGuardians.indexOf(loadedGuardian)+1 == loadedGuardians.size() )
            return true;
        else
            return false;
    }
    
    public boolean loadedFirst() throws Exception {
        if ( loadedGuardian == null || loadedGuardians == null || loadedGuardians.size() == 0)
            throw new Exception("Guardians not loaded");
        if ( loadedGuardians.indexOf(loadedGuardian) == 0 )
            return true;
        else
            return false;
    }
    
    public GuardianDao(Database database) 
    {
        this.setDatabase(database);
        loadedGuardians = new ArrayList<Guardian>();
    }

    public static void setDatabase(Database database) 
    {
        GuardianDao.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Guardian.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Guardian.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    } 

    public int create(Guardian guardian) 
    {
        int nrows = 0;
        try {
            if (dao.queryForId(guardian.getGuardian_nidc()) != null)
                throw new SQLException("Error: object already saved");
            else {
                nrows = dao.create(guardian);
                if ( nrows == 0 )
                    throw new SQLException("Error: object not saved");
                this.loadedGuardian = guardian;
                loadedGuardians.add(guardian);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return nrows;
    } 

    public int update(Guardian guardian) {
        int nrows = 0;
        try {
            if (dao.queryForId(guardian.getGuardian_nidc()) != null)
                throw new SQLException("Error: object already saved");
            else {
                nrows = dao.update(guardian); 
                if ( nrows == 0 )
                    throw new SQLException("Error: object not updated");
                this.loadedGuardian.setGuardian_nidc(guardian.getGuardian_nidc());
                this.loadedGuardian.setFullName(guardian.getFullName());
                this.loadedGuardian.setBirthday(guardian.getBirthday());
                this.loadedGuardian.setGuardian_phone(guardian.getGuardian_phone());
                this.loadedGuardian.setFingerprint(guardian.getFingerprint());
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return nrows;
    }

    public int delete(Guardian guardian) 
    {
        int nrows = 0;
        try {
            if (dao.queryForId(guardian.getGuardian_nidc()) != null)
                throw new SQLException("Error: object already saved");
            else {
                nrows = dao.delete(guardian);
                if ( nrows == 0 )
                    throw new SQLException("Error: object not deleted"); 
                int index = this.loadedGuardians.indexOf(this.loadedGuardian);
                this.loadedGuardians.remove(this.loadedGuardian);
            }
        } catch (SQLException e) {
             System.out.println(e);
        }
        return nrows;
    }   

    public Guardian loadFromNidc(int nidc) {
        try {
            this.loadedGuardian = dao.queryForId(nidc);
            if (this.loadedGuardian != null)
                this.loadedGuardians.add(this.loadedGuardian);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedGuardian;
    }    
    
    public List<Guardian> loadAll() {
        try {
            this.loadedGuardians =  dao.queryForAll();
            if (this.loadedGuardians.size() != 0)
                this.loadedGuardian = this.loadedGuardians.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.loadedGuardians;
    }
    

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie database*/
    public Database getDatabase(){
        return this.database;
    }//end method getDatabase

    /**GET Method Propertie dao*/
    public com.j256.ormlite.dao.Dao<model.entity.Guardian, java.lang.Integer> getDao(){
        return this.dao;
    }//end method getDao

    /**SET Method Propertie dao*/
    public void setDao(com.j256.ormlite.dao.Dao<model.entity.Guardian, java.lang.Integer> dao){
        this.dao = dao;
    }//end method setDao

    /**GET Method Propertie loadedGuardians*/
    public java.util.List<model.entity.Guardian> getLoadedGuardians(){
        return this.loadedGuardians;
    }//end method getLoadedGuardians

    /**SET Method Propertie loadedGuardians*/
    public void setLoadedGuardians(java.util.List<model.entity.Guardian> loadedGuardians){
        this.loadedGuardians = loadedGuardians;
    }//end method setLoadedGuardians

    /**GET Method Propertie loadedGuardian*/
    public Guardian getLoadedGuardian(){
        return this.loadedGuardian;
    }//end method getLoadedGuardian

    /**SET Method Propertie loadedGuardian*/
    public void setLoadedGuardian(Guardian loadedGuardian){
        this.loadedGuardian = loadedGuardian;
    }//end method setLoadedGuardian

    /**GET Method Propertie nidcs*/
    public java.util.ArrayList<java.lang.String> getNidcs(){
        return this.nidcs;
    }//end method getNidcs

    /**SET Method Propertie nidcs*/
    public void setNidcs(java.util.ArrayList<java.lang.String> nidcs){
        this.nidcs = nidcs;
    }//end method setNidcs

    //End GetterSetterExtension Source Code
//!
}