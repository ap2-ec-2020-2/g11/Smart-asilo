package view;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import java.awt.Dimension;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.FlowLayout;

import model.entity.ResidentGuardian;
import model.entity.Nutrition;
import controller.NutritionController;
import model.entity.Resident;
import controller.ResidentController;
import model.entity.Guardian;
import controller.GuardianController;
import model.entity.Visitor;
import controller.VisitorController;
import model.entity.Professional;
import controller.ProfessionalController;
import model.entity.Pharmacy;
import controller.PharmacyController;
import model.entity.Maintenance;
import controller.MaintenanceController;
import model.Database;

import view.util.CrudToolBar;
import view.util.FormUtils;
/**
 * Escreva a descrição da classe ProjectView aqui.
 * 
 * @author
*/
public class ProjectView
{
    public ProjectView() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        loginFormPanel = new LoginFormPanel();
        occuppational_areaFormPanel = new Occuppational_areaFormPanel();
        registrationFormPanel = new RegistrationFormPanel();
        recordFormPanel = new RecordFormPanel();
        financialExpensesFormPanel = new FinancialExpensesFormPanel();
        
        //setLayout(new java.awt.BorderLayout());
        //add(buttonFormPanel, java.awt.BorderLayout.PAGE_START);
        //add(professionalFormPanel, java.awt.BorderLayout.CENTER);
    }// </editor-fold>                        


    // Variables declaration - do not modify                     
    private LoginFormPanel loginFormPanel;
    private Occuppational_areaFormPanel occuppational_areaFormPanel;
    private RegistrationFormPanel registrationFormPanel;
    private RecordFormPanel recordFormPanel;
    private FinancialExpensesFormPanel financialExpensesFormPanel;
    // End of variables declaration
    
    public static void main(String[] args) {
        javax.swing.JFrame jFrame = new javax.swing.JFrame();
        jFrame.setSize(300,300);
        LoginFormPanel srv = new LoginFormPanel();
        jFrame.add(srv);
        jFrame.setVisible(true);
        jFrame.pack();
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie loginFormPanel*/
    public LoginFormPanel getLoginFormPanel(){
        return this.loginFormPanel;
    }//end method getLoginFormPanel

    /**SET Method Propertie loginFormPanel*/
    public void setLoginFormPanel(LoginFormPanel loginFormPanel){
        this.loginFormPanel = loginFormPanel;
    }//end method setLoginFormPanel

    /**GET Method Propertie occuppational_areaFormPanel*/
    public Occuppational_areaFormPanel getOccuppational_areaFormPanel(){
        return this.occuppational_areaFormPanel;
    }//end method getOccuppational_areaFormPanel

    /**SET Method Propertie occuppational_areaFormPanel*/
    public void setOccuppational_areaFormPanel(Occuppational_areaFormPanel occuppational_areaFormPanel){
        this.occuppational_areaFormPanel = occuppational_areaFormPanel;
    }//end method setOccuppational_areaFormPanel

    /**GET Method Propertie registrationFormPanel*/
    public RegistrationFormPanel getRegistrationFormPanel(){
        return this.registrationFormPanel;
    }//end method getRegistrationFormPanel

    /**SET Method Propertie registrationFormPanel*/
    public void setRegistrationFormPanel(RegistrationFormPanel registrationFormPanel){
        this.registrationFormPanel = registrationFormPanel;
    }//end method setRegistrationFormPanel

    /**GET Method Propertie recordFormPanel*/
    public RecordFormPanel getRecordFormPanel(){
        return this.recordFormPanel;
    }//end method getRecordFormPanel

    /**SET Method Propertie recordFormPanel*/
    public void setRecordFormPanel(RecordFormPanel recordFormPanel){
        this.recordFormPanel = recordFormPanel;
    }//end method setRecordFormPanel

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie financialExpensesFormPanel*/
    public FinancialExpensesFormPanel getFinancialExpensesFormPanel(){
        return this.financialExpensesFormPanel;
    }//end method getFinancialExpensesFormPanel

    /**SET Method Propertie financialExpensesFormPanel*/
    public void setFinancialExpensesFormPanel(FinancialExpensesFormPanel financialExpensesFormPanel){
        this.financialExpensesFormPanel = financialExpensesFormPanel;
    }//end method setFinancialExpensesFormPanel

    //End GetterSetterExtension Source Code
//!
}