O projeto é um protótipo voltado para uma adaptação mais segura e desempenho inteligente dos processos de registros e dados de asilos, por meio de um sistema integrado. O protótipo conta com uma hierarquia de acesso dividida entre um administrador geral, o qual é permitido todo o manejo dos dados do sistema, e departamentos, que contam com gerenciadores limitados ao acesso dos dados de seus respectivos departamentos e funcionários responsáveis exclusivamente por suas funções.

 O projeto por se tratar de um minímo produto viável, um protótipo, ele não foi finalizado, apresentando apenas suas ideias principais e pretende se aperfeiçoar e finalizar seu desenvolvimento para sua completa funcionalidade.

Integrantes

Emanuelle Passos Martins
@emanuellepassos

Bianca Pereira de Carvalho
@bainca

Joyce Cardoso de Araújo
@joycecardoso

Débora Eliane Soares de Souza Batista
@Debora_eliane
